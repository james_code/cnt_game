﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.View.Form.LettersForm" 
 * </copyright>
 * ========================================================================
 * LettersForm 
 * Administrator 
 * 2020/3/14 21:38:10 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.View.Form
{
    /// <summary>
    /// 
    /// </summary>
    public class LettersForm : INotifyPropertyChanged
    {
        private string _ch_1;

        private string _ch_2;

        private string _ch_3;

        private string _ch_4;

        private string _ch_5;

        private string _ch_6;

        private string _ch_7;

        private string _ch_8;

        private string _ch_9;

        public string Ch_1 {
            get { return _ch_1; }
            set
            {
                _ch_1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_1"));
            }
        }
        public string Ch_2 {
            get { return _ch_2; }
            set
            {
                _ch_2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_2"));
            }
        }
        public string Ch_3 {
            get { return _ch_3; }
            set
            {
                _ch_3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_3"));
            }
        }
        public string Ch_4 {
            get { return _ch_4; }
            set
            {
                _ch_4 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_4"));
            }
        }
        public string Ch_5 {
            get { return _ch_5; }
            set
            {
                _ch_5 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_5"));
            }
        }
        public string Ch_6 {
            get { return _ch_6; }
            set
            {
                _ch_6 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_6"));
            }
        }
        public string Ch_7 {
            get { return _ch_7; }
            set
            {
                _ch_7 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_7"));
            }
        }
        public string Ch_8 {
            get { return _ch_8; }
            set
            {
                _ch_8 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_8"));
            }
        }
        public string Ch_9 {
            get { return _ch_9; }
            set
            {
                _ch_9 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_ch_9"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
