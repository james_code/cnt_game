﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.View.Form.BaseForm" 
 * </copyright>
 * ========================================================================
 * BaseForm 
 * Administrator 
 * 2020/3/15 11:08:49 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.View.Form
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseForm : INotifyPropertyChanged
    {
        private int roundNum;
        public int RoundNum
        {
            get { return roundNum; }
            set
            {
                roundNum = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RoundNum"));
            }
        }

        private int totalPoint;
        public int TotalPoint
        {
            get { return totalPoint; }
            set
            {
                totalPoint = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TotalPoint"));
            }
        }

        private string roundOrd;
        public string RoundOrd
        {
            get { return roundOrd; }
            set
            {
                roundOrd = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RoundOrd"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
