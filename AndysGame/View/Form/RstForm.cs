﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.View.Form.RstForm" 
 * </copyright>
 * ========================================================================
 * RstForm 
 * Administrator 
 * 2020/3/15 10:54:05 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.View.Form
{
    /// <summary>
    /// 
    /// </summary>
    public class RstForm : BaseForm
    {
        private string answerRst;

        public string AnswerRst
        {
            get { return answerRst; }
            set
            {
                answerRst = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AnswerRst"));
            }
        }

        private string rightAnswers;

        public string RightAnswers
        {
            get { return rightAnswers; }
            set
            {
                rightAnswers = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RightAnswers"));
            }
        }

        private int answerPoint;

        public int AnswerPoint
        {
            get { return answerPoint; }
            set
            {
                answerPoint = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AnswerPoint"));
            }
        }
        private string _ch_1;

        private string _ch_2;

        private string _ch_3;

        private string _ch_4;

        public string _Ch_1
        {
            get { return _ch_1; }
            set
            {
                _ch_1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_Ch_1"));
            }
        }
        public string _Ch_2
        {
            get { return _ch_2; }
            set
            {
                _ch_2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_Ch_2"));
            }
        }
        public string _Ch_3
        {
            get { return _ch_3; }
            set
            {
                _ch_3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_Ch_3"));
            }
        }
        public string _Ch_4
        {
            get { return _ch_4; }
            set
            {
                _ch_4 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("_Ch_4"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    

    }
}
