﻿using AndysGame.Const;
using AndysGame.Enum;
using AndysGame.View.Form;
using AndysGame.View.Select;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AndysGame.View.Round
{
    /// <summary>
    /// RoundWindow.xaml
    /// </summary>
    public partial class RoundWindow : Window
    {
        private RstForm rstForm;

        public RoundWindow()
        {
            InitializeComponent();
        }

        public RoundWindow(RstForm aRstForm)
        {
            InitializeComponent();

            this.rstForm = aRstForm;

            this.DataContext = this.rstForm;

        }

        private void continue_Click(object sender, RoutedEventArgs e)
        {
            SelectWindow selectWindow = new SelectWindow(rstForm);
            selectWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            selectWindow.Left = this.Left;
            selectWindow.Top = this.Top;
            selectWindow.Show();
            this.Close();
        }
    }


}
