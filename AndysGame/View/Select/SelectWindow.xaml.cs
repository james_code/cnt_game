﻿using AndysGame.Enum;
using AndysGame.Util;
using AndysGame.View.Form;
using AndysGame.View.Play;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AndysGame.View.Select
{
    /// <summary>
    /// SelectWindow.xaml
    /// </summary>
    public partial class SelectWindow : Window
    {
        private int clickNum = 0;
        private Letters letters = new Letters();
        private RstForm rstForm;

        public SelectWindow()
        {
            InitializeComponent();
        }

        public SelectWindow(RstForm aRstForm)
        {
            InitializeComponent();

            this.rstForm = aRstForm;
        }

        private void VowBtn_Click(object sender, RoutedEventArgs e)
        {
            clickNum++;

            if ( clickNum > 9 )
            {
                return;
            }

            int theNum = RandomUtil.GetMyNumByRandom(5);
            string aVowel = ((Vowel)theNum).ToString();

            letters.SelectedLetters.Add(aVowel);

            FillTheTxt(aVowel);

            if (clickNum == 9)
            {

                ctnBtn.Visibility = Visibility.Visible;
                vowBtn.Visibility = Visibility.Hidden;
                conBtn.Visibility = Visibility.Hidden;
            }



        }

        private void ConBtn_Click(object sender, RoutedEventArgs e)
        {
            ///
            Console.WriteLine("1:" + Environment.CurrentDirectory);
            Console.WriteLine("2:" + Directory.GetCurrentDirectory());

            clickNum++;

            if (clickNum > 9)
            {
                return;
            }

            int theNum = RandomUtil.GetMyNumByRandom(21);
            string aConsonant = ((Consonant)theNum).ToString();

            letters.SelectedLetters.Add(aConsonant);
            FillTheTxt(aConsonant);

            if (clickNum == 9)
            {
                ctnBtn.Visibility = Visibility.Visible;
                vowBtn.Visibility = Visibility.Hidden;
                conBtn.Visibility = Visibility.Hidden;
            }

        }


        private void FillTheTxt(string theLetter)
        {
            switch (clickNum)
            {
                case 1:
                    _ch_1.Text = theLetter;
                    break;
                case 2:
                    _ch_2.Text = theLetter;
                    break;
                case 3:
                    _ch_3.Text = theLetter;
                    break;
                case 4:
                    _ch_4.Text = theLetter;
                    break;
                case 5:
                    _ch_5.Text = theLetter;
                    break;
                case 6:
                    _ch_6.Text = theLetter;
                    break;
                case 7:
                    _ch_7.Text = theLetter;
                    break;
                case 8:
                    _ch_8.Text = theLetter;
                    break;
                case 9:
                    _ch_9.Text = theLetter;
                    break;
                default:
                    Console.WriteLine("===============The letter matched nothing!============");
                    break;
            }

            Console.WriteLine(string.Format(@"===============the Letter:{0}, clickNum:{1}============", theLetter, clickNum));
        }

        private async void CtnBtn_ClickAsync(object sender, RoutedEventArgs e)
        {
            ctnBtn.IsEnabled = false;
            ctnBtn.Content = "Waiting...";

            try
            {
                await letters.FilterTheWordsByLetters();

                _text.Text = "Emm, I am identifing your answer by your selection...";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(ex.Message);
                return;
            }

            LettersForm lettersForm = new LettersForm();
            lettersForm.Ch_1 = _ch_1.Text;
            lettersForm.Ch_2 = _ch_2.Text;
            lettersForm.Ch_3 = _ch_3.Text;
            lettersForm.Ch_4 = _ch_4.Text;
            lettersForm.Ch_5 = _ch_5.Text;
            lettersForm.Ch_6 = _ch_6.Text;
            lettersForm.Ch_7 = _ch_7.Text;
            lettersForm.Ch_8 = _ch_8.Text;
            lettersForm.Ch_9 = _ch_9.Text;

            PlayWindow playWindow = new PlayWindow(rstForm,lettersForm, this.letters);
            playWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            playWindow.Left = this.Left;
            playWindow.Top = this.Top;
            playWindow.Show();
            this.Close();
        }

    }
}
