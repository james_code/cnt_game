﻿using AndysGame.Enum;
using AndysGame.Util;
using AndysGame.View.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AndysGame.View.Play
{
    
    
    
    /// <summary>
    /// PlayWindow.xaml
    /// </summary>
    public partial class PlayWindow : Window
    {
        private LettersForm aPlayForm;
        private Letters aletters;
        private RstForm rstForm;
        private DispatcherTimer readDataTimer = new DispatcherTimer();
        private int timeTotal = 30;

        public PlayWindow()
        {
            InitializeComponent();
        }

        public PlayWindow(RstForm aRstForm,LettersForm lettersForm, Letters letters)
        {
            this.rstForm = aRstForm;
            this.aPlayForm = lettersForm;
            this.aletters = letters;

            InitializeComponent();

            InitText();

            Timer_Controller();

        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            _answerText.Text = "";
            _ch_1.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_1.IsEnabled = true;

            _ch_2.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_2.IsEnabled = true;

            _ch_3.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_3.IsEnabled = true;

            _ch_4.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_4.IsEnabled = true;

            _ch_5.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_5.IsEnabled = true;

            _ch_6.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_6.IsEnabled = true;

            _ch_7.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_7.IsEnabled = true;

            _ch_8.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_8.IsEnabled = true;

            _ch_9.Background = new SolidColorBrush(Color.FromRgb(58, 80, 216));
            _ch_9.IsEnabled = true;
        }

        private void InitText() {
            _ch_1.Text = this.aPlayForm.Ch_1;
            _ch_2.Text = this.aPlayForm.Ch_2;
            _ch_3.Text = this.aPlayForm.Ch_3;
            _ch_4.Text = this.aPlayForm.Ch_4;
            _ch_5.Text = this.aPlayForm.Ch_5;
            _ch_6.Text = this.aPlayForm.Ch_6;
            _ch_7.Text = this.aPlayForm.Ch_7;
            _ch_8.Text = this.aPlayForm.Ch_8;
            _ch_9.Text = this.aPlayForm.Ch_9;

        }

        private void GiveTheCrazyAnswers()
        {
            if (this.aletters != null && this.aletters.TheLongsetWordList != null
                    && this.aletters.TheLongsetWordList.Count > 0)
            {
                rstForm.RightAnswers = ToolUtil.ListToString(this.aletters.TheLongsetWordList);
            }
                

        }

        private void _ch_1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_1.Text;
            _ch_1.Background = new SolidColorBrush(Color.FromRgb(102,117,209));
            _ch_1.IsEnabled = false;
        }

        private void _ch_2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_2.Text;
            _ch_2.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_2.IsEnabled = false;
        }

        private void _ch_3_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_3.Text;
            _ch_3.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_3.IsEnabled = false;
        }

        private void _ch_4_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_4.Text;
            _ch_4.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_4.IsEnabled = false;
        }

        private void _ch_5_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_5.Text;
            _ch_5.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_5.IsEnabled = false;
        }

        private void _ch_6_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_6.Text;
            _ch_6.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_6.IsEnabled = false;
        }

        private void _ch_7_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_7.Text;
            _ch_7.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_7.IsEnabled = false;
        }

        private void _ch_8_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_8.Text;
            _ch_8.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_8.IsEnabled = false;
        }

        private void _ch_9_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _answerText.Text += _ch_9.Text;
            _ch_9.Background = new SolidColorBrush(Color.FromRgb(102, 117, 209));
            _ch_9.IsEnabled = false;
        }


        private void Timer_Controller() {

            _timer.Text = Convert.ToString(this.timeTotal);
            readDataTimer.Tick += new EventHandler(timeCycle);
            readDataTimer.Interval = new TimeSpan(0, 0, 0, 1);
            readDataTimer.Start();

        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            ChangeToNextRound();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timeCycle(object sender, EventArgs e)
        {
            this.timeTotal--;
            _timer.Text = Convert.ToString(this.timeTotal);

            ///time out, do sth
            if (this.timeTotal == 0) {
                
                ChangeToNextRound();
            }

            if (this.aletters!=null && this.aletters.TheLongsetWordList!=null
                    && this.aletters.TheLongsetWordList.Count > 0) {
                submitBtn.Visibility = Visibility.Visible;
            }

        }

        private void ChangeToNextRound() {

            readDataTimer.Stop();
            this.timeTotal = 30;

            string answerText = _answerText.Text;

            if (ToolUtil.isInList(this.aletters.TheLongsetWordList, answerText))
            {
                rstForm.AnswerRst = "Right";
                rstForm.AnswerPoint = 10;
                rstForm.TotalPoint += 10;
                FillRstCard(rstForm, true);
            }
            else
            {
                rstForm.AnswerRst = "Wrong";
                rstForm.AnswerPoint = 0;
                FillRstCard(rstForm, false);
            }

            GiveTheCrazyAnswers();

            ///to the result
            ResultWindow resultWindow = new ResultWindow(rstForm);
            resultWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            resultWindow.Left = this.Left;
            resultWindow.Top = this.Top;
            resultWindow.Show();
            this.Close();
        }

        private void FillRstCard(RstForm rstForm, Boolean win) {

            switch (rstForm.RoundNum) {
                case 1:
                    if (win) {
                        rstForm._Ch_1 = "R";
                    }
                    else {
                        rstForm._Ch_1 = "X";
                    }
                    break;
                case 2:
                    if (win)
                    {
                        rstForm._Ch_2 = "R";
                    }
                    else
                    {
                        rstForm._Ch_2 = "X";
                    }
                    break;
                case 3:
                    if (win)
                    {
                        rstForm._Ch_3 = "R";
                    }
                    else
                    {
                        rstForm._Ch_3 = "X";
                    }
                    break;
                case 4:
                    if (win)
                    {
                        rstForm._Ch_4 = "R";
                    }
                    else
                    {
                        rstForm._Ch_4 = "X";
                    }
                    break;
                default:
                    MessageBox.Show("Bugs!Cannot Find Your Round Num");
                    break;

            }

        }

    }
}
