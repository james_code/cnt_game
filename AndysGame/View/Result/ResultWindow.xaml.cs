﻿using AndysGame.Enum;
using AndysGame.Util;
using AndysGame.View.Form;
using AndysGame.View.Round;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AndysGame.View
{
    

    /// <summary>
    /// ResultWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ResultWindow : Window
    {
        private RstForm rstForm;

        public ResultWindow()
        {
            InitializeComponent();
        }

        public ResultWindow(RstForm aRstForm)
        {
            InitializeComponent();

            this.rstForm = aRstForm;

            if (aRstForm.RoundNum >= 4)
            {
                _contBtn.Visibility = Visibility.Hidden;
                _retBtn.Visibility = Visibility.Visible;
            }
            else {
                _contBtn.Visibility = Visibility.Visible;
                _retBtn.Visibility = Visibility.Hidden;
            }

            //textBoxAnswers.Text = this.rstForm.RightAnswers;

            this.DataContext = this.rstForm;
        }

        private void Continue_Click(object sender, RoutedEventArgs e)
        {
            rstForm.RoundNum += 1;
            rstForm.RoundOrd = ((RoundOfOrder)this.rstForm.RoundNum).ToString();
            RoundWindow resultWindow = new RoundWindow(rstForm);
            resultWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            resultWindow.Left = this.Left;
            resultWindow.Top = this.Top;
            resultWindow.Show();
            this.Close();
        }

        private void _retBtn_Click(object sender, RoutedEventArgs e)
        {
            rstForm.RoundNum = 1;
            rstForm.RoundOrd = ((RoundOfOrder)1).ToString();
            rstForm.TotalPoint = 0;
            rstForm._Ch_1 = "";
            rstForm._Ch_2 = "";
            rstForm._Ch_3 = "";
            rstForm._Ch_4 = "";
            rstForm.RightAnswers = "";

            RoundWindow resultWindow = new RoundWindow(rstForm);
            resultWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            resultWindow.Left = this.Left;
            resultWindow.Top = this.Top;
            resultWindow.Show();
            this.Close();
        }
    }
}
