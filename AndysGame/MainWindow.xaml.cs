﻿using AndysGame.Enum;
using AndysGame.View.Form;
using AndysGame.View.Round;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AndysGame
{
    /// <summary>
    /// MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private RstForm rstForm = new RstForm();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void start_Click(object sender, RoutedEventArgs e)
        {
            rstForm.RoundNum = 1;
            rstForm.RoundOrd = ((RoundOfOrder)1).ToString();

            RoundWindow roundWindow = new RoundWindow(rstForm);
            roundWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            roundWindow.Left = this.Left;
            roundWindow.Top = this.Top;
            roundWindow.Show();
            this.Close();

        }
    }
}
