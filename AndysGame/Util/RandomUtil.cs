﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.Util.RandomUtil" 
 * </copyright>
 * ========================================================================
 * RandomUtil 
 * Administrator 
 * 2020/3/14 11:00:12 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.Util
{
    /// <summary>
    /// 
    /// </summary>
    class RandomUtil
    {
        public static int GetMyNumByRandom(int theMax) {

            Random rd = new Random();
            int theNum = rd.Next(0, theMax-1);
            Console.WriteLine(string.Format(@"==================The Random Num is:{0}=============================", theNum));
            return theNum;
        }

    }
}
