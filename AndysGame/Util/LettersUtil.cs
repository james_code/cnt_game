﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.Util.LettersUtil" 
 * </copyright>
 * ========================================================================
 * LettersUtil 
 * Administrator 
 * 2020/3/14 11:23:04 
 * 
 * =====================================================================
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.Util
{
    /// <summary>
    /// 
    /// </summary>
    class LettersUtil
    {

        public static List<string> ReOrderFromLongToShort(List<string> origList) {

            List<string> theNewList = new List<string>();

            theNewList = origList.OrderByDescending(t => t.Length).ToList();

            return theNewList;
        }

        public static List<string> FilterWords(List<string> lettersList, List<string> wordsList) {

            List<string> theNewList = new List<string>();

            IEnumerator rator = wordsList.GetEnumerator();

            while (rator.MoveNext())
            {
                string aWord = (string)rator.Current;
                
                if (aWord==null || aWord.Trim().Length==0 
                      || !ToolUtil.isAllAlp(aWord))
                {
                    continue;
                }

                if (theNewList!=null && theNewList.Count>0
                        && theNewList[0].Length>aWord.Length) {
                    break;
                }

                aWord = aWord.ToUpper();

                Console.WriteLine(string.Format(@"================== the current word  is: {0}", aWord));

                string aWordTmp = aWord.ToUpper();

                int wordMaxLength = 0;

                //string[] testArray = { "N","O","I","T","C","E","L","E","S"};
                //lettersList = testArray.ToList<string>();

                for (int i=0;i<lettersList.Count;i++) {

                    string aLetter = lettersList[i].ToUpper();

                    if (aWordTmp.Contains(aLetter))
                    {
                        int index = aWordTmp.IndexOf(aLetter);

                        aWordTmp = ReplaceChar(aWordTmp,index);
                        continue;
                    }

                }

                if ( aWordTmp.Length==0 ) {

                    if (theNewList == null || theNewList.Count == 0)
                    {
                        wordMaxLength = aWord.Length;
                        theNewList.Add(aWord);
                    }
                    else if(theNewList[0].Length < aWord.Length)
                    {
                        wordMaxLength = aWord.Length;
                        theNewList.Clear();
                        theNewList.Add(aWord);
                    }
                    else if (theNewList[0].Length == aWord.Length)
                    {
                        theNewList.Add(aWord);
                    }


                }

                
            }

            return theNewList;
        }

        public static string ReplaceChar(string oldStr, int index) {
            char[] c = oldStr.ToArray();
            List<char> cList = c.ToList<char>();

            cList.RemoveAt(index);
            c = cList.ToArray<char>();

            string newStr = new string(c);
            
            return newStr;

        }
    }
}
