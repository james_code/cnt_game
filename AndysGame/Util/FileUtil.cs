﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.Util.FileUtil" 
 * </copyright>
 * ========================================================================
 * FileUtil 
 * Administrator 
 * 2020/3/14 15:33:23 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.Util
{
    /// <summary>
    /// 
    /// </summary>
    class FileUtil
    {
        /// <summary>
        /// File.words.txt
        /// 
        /// </summary>
        /// <param name="fileNm"></param>
        /// <param name="filePathInside"></param>
        public static void CopyFileFromInside(string filePathInside, 
            string fileOutPath) {

            BufferedStream inStream = null;
            FileStream outStream = null;

            try
            {
                Assembly assm = Assembly.GetExecutingAssembly();
                Stream istr = assm.GetManifestResourceStream("AndysGame." + filePathInside);

                inStream = new BufferedStream(istr);

                FileInfo fi = new FileInfo(fileOutPath);
                var di = fi.Directory;
                if (!di.Exists) {
                    di.Create();
                }

                outStream = new FileStream(fileOutPath, FileMode.Create, FileAccess.Write);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = inStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    outStream.Write(buffer, 0, length);
                }

                outStream.Flush();
            }
            finally
            {
                if (outStream != null) {
                    outStream.Close();
                }

                if (inStream != null) {
                    inStream.Close();
                }
            }

          
        }

    }
}
