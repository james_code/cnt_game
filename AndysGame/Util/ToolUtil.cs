﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.Util.ToolUtil" 
 * </copyright>
 * ========================================================================
 * ToolUtil 
 * Administrator 
 * 2020/3/15 10:37:53 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AndysGame.Util
{
    /// <summary>
    /// 
    /// </summary>
    class ToolUtil
    {

        public static Boolean isInList<T>(List<T> list, T element)
        {

            if (element == null) return false;

            if (list.Contains(element))
            {
                return true;
            }

            return false;
        }

        public static string ListToString<T>(List<T> list)
        {
            if (list == null || list.Count == 0) return "";

            StringBuilder sb = new StringBuilder();

            foreach (T t in list)
            {
                string str = Convert.ToString(t);
                sb.Append(str + ";");
            }

            return sb.ToString();

        }


        public static bool isAllAlp(string str)
        {
            string pattern = @"^[A-Za-z]+$";

            Match match = Regex.Match(str, pattern);

            return match.Success;

        }

    }

}
