﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.Enum.Letters" 
 * </copyright>
 * ========================================================================
 * Letters 
 * Administrator 
 * 2020/3/14 10:46:32 
 * 
 * =====================================================================
*/
using AndysGame.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.Enum
{
    public enum Vowel {

        A = 0,
        E = 1,
        I = 2,
        O = 3,
        U = 4

    }

    public enum Consonant {

        B = 0,
        C = 1,
        D = 2,
        F = 3,
        G = 4,
        H = 5,
        J = 6,
        K = 7,
        L = 8,
        M = 9,
        N = 10,
        P = 11,
        Q= 12,
        R = 13,
        S = 14,
        T = 15,
        V = 16,
        W = 17,
        X = 18,
        Y = 19,
        Z = 20
    }

    /// <summary>
    /// 
    /// </summary>
    public class Letters
    {

        private List<string> selectedLetters = new List<string>();
        private List<string> theLongsetWordList = new List<string>();
        
        private static string filePath = AppDomain.CurrentDomain.BaseDirectory + @"\File\words.txt";

        private static string[] lines = ReadAllWordToGC();

        private static string[] ReadAllWordToGC() {
            if (!File.Exists(filePath))
            {

                try
                {
                    FileUtil.CopyFileFromInside("File.words.txt", filePath);
                }
                catch (Exception)
                {

                    throw new Exception("No Words Sample File can be identfied");
                }

            }

            string[] newLines = File.ReadAllLines(filePath);

            File.Delete(filePath);

            return newLines;

        }

        /// <summary>
        /// 
        /// </summary>
        public async Task FilterTheWordsByLetters()
        {
            Console.WriteLine(string.Format(@"=============== selectedLetters count A Leeter:{0}", selectedLetters.Count));

            if (lines == null || lines.Length==0) {
                lines = ReadAllWordToGC();
            }


            List<string> listS = new List<string>(lines);

            var query = from words in listS where words.Length>0 && words.Length<10 select words;

            List<string> _aResult_0  = query.ToList<string>();

            List<string> _aResult_1 = LettersUtil.ReOrderFromLongToShort(_aResult_0);

            var rst = await Task.Run(()=> LettersUtil.FilterWords(selectedLetters,_aResult_1));

            theLongsetWordList = (List<string>)rst;

            Console.WriteLine(string.Format(@"================== the theLongsetWordList's count: {0}", theLongsetWordList.Count));
            Console.WriteLine(string.Format(@"================== theLongsetWordList's list: {0}", ToolUtil.ListToString(theLongsetWordList)));
        }

        public List<string> SelectedLetters { get => selectedLetters; set => selectedLetters = value; }
        public List<string> TheLongsetWordList { get => theLongsetWordList; set => theLongsetWordList = value; }
    }
}
