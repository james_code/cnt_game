﻿/*
 * ========================================================================
 * <copyright> 
        author="James CHEN"  2020-2020 
        path="AndysGame.Enum.Class1" 
 * </copyright>
 * ========================================================================
 * Class1 
 * Administrator 
 * 2020/3/13 23:00:44 
 * 
 * =====================================================================
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndysGame.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum RoundOfOrder
    {
        first = 1,
        second =2,
        third = 3,
        forth = 4

    }
}
